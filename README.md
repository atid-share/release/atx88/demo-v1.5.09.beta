## RELEASE HISTORY: AtidReader Demo  
#### _Release date: 2021-12-03_

#### {+Demo version : ATID Reader SE Demo_v1.5.09.2021120300.beta+}

#### Suported FW version: AT388_bd-2.2.2.6-9 and above

#### What's New:
- Rail Tag Inventory Feature added
  * by default 6C tag inventory is set.
       [ Settings - > Inventory Tag Type -> (6C/6B/Rail)]
      <details>
     <summary markdown="span" >Screenshots</summary>
      <div align="left">
        <img width="22%" src="https://gitlab.com/atid-share/asset/-/raw/834c9382feb6e4231fe8887b2faae8050fc84097/settings-00.png"  alt="Setting screen" title="Settings"</img>
        <img width="22%" src="https://gitlab.com/atid-share/asset/-/raw/834c9382feb6e4231fe8887b2faae8050fc84097/settings-01.png" alt="List screen" title="Setting Lists"></img>
        <img width="22%" src="https://gitlab.com/atid-share/asset/-/raw/834c9382feb6e4231fe8887b2faae8050fc84097/settings-02.png" alt="dialog " title="Option dialog"></img>
       </div>
      </details>
